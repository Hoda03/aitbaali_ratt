package tp1
class BootStrap {
    def init = { servletContext ->
        def roleAdmin = new Role(authority: "ROLE_ADMIN").save()
        def roleUser = new Role(authority: "ROLE_USER").save()
        def userAdmin = new User(username: "admin", password: "password").save()
        def userClient = new User(username: "user", password: "password").save()
        def roleModerator = new Role(authority: "ROLE_MODERATOR").save()
        def userModerator = new User(username: "moderator", password: "password").save()

        UserRole.create(userAdmin, roleAdmin, true)
        UserRole.create(userClient, roleUser, true)
        UserRole.create(userModerator, roleModerator, true)

        (1..5).each {
            Integer index ->
                def annonceInstance = new SaleAd(
                        title: "Annonce" + index,
                        description: "description " + index,
                        longDescription: "description longue " + index,
                        price: 100F * index)
                (1..3).each {
                    annonceInstance.addToIllustrations(new Illustration(filename: "illustration"+(it+(index-1)*3)+".jpg"))
                }
                userClient.addToSaleAds(annonceInstance)
        }
        userClient.save(flush: true, failOnError: true)
    }
    def destroy = {
    }

}