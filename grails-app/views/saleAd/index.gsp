<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'saleAd.label', default: 'SaleAd')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>

    </head>
    <body>
    <style>
    body
    {
        background-color: #2f4f4f;
    }
    </style>
        <a href="#list-saleAd" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="list-saleAd" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <table>
                <thead>
                <tr>
                    <th class="sortable" ><a href="/tp1/saleAd/index?sort=title&amp;max=10&amp;order=asc">Title</a></th>
                    <th class="sortable" ><a href="/tp1/saleAd/index?sort=description&amp;max=10&amp;order=asc">Description</a></th>
                    <th class="sortable" ><a href="/tp1/saleAd/index?sort=longDescription&amp;max=10&amp;order=asc">Long Description</a></th>
                    <th class="sortable" ><a href="/tp1/saleAd/index?sort=illustrations&amp;max=10&amp;order=asc">Illustrations</a></th>
                    <th class="sortable" ><a href="/tp1/saleAd/index?sort=price&amp;max=10&amp;order=asc">Price</a></th>
                    <th class="sortable" ><a href="/tp1/saleAd/index?sort=author&amp;max=10&amp;order=asc">Author</a></th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${saleAdList}" var="saleAd">
                    <tr class="even">


                        <td><a href="${grailsApplication.config.tp1.illustrations.grailsurl + "saleAd/show/" + saleAd.id}">${saleAd.title}</a></td>
                        <td>${saleAd.description}</td>
                        <td>${saleAd.longDescription}</td>
                        <td><ul><li>
                            <g:each in="${saleAd.illustrations}" var="illustration" status="i">
                                <g:if test="${i <= 3}">
                                    <img src="${grailsApplication.config.tp1.illustrations.url + illustration.filename}">
                                </g:if>
                            </g:each>
                        </li></ul></td>
                        <td>${saleAd.price}</td>
                        <td>${saleAd.author.username}</td>
                    </tr>
                </g:each>
                </tbody>
            </table>

            <div class="pagination">

            </div>
        </div>


    </body>
</html>