<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Grails</title>

</head>
<body>
<style>
body
{
    background-color: #545454;
}
</style>
<g:link controller="login"><font color="black" size="5pt">Connexion</font></g:link>
<asset:image src="OIP.jpg" height="20" width="20"/>

<g:link controller="logout"><font color="black" size="5pt">Déconnexion</font></g:link>
<asset:image src="logout.png" height="20" width="20"/>

<g:link controller="saleAd"><font color="black" size="5pt">Annonces</font></g:link>
<asset:image src="annonce.jpg" height="30" width="30"/>

<g:link controller="user"><font color="black" size="5pt">Utilisateurs</font></g:link>
<asset:image src="user.jpg" height="30" width="30"/>

<content tag="nav">


<br>
<div class="svg" role="presentation">
    <div class="grails-logo-container">
        <asset:image src="illustration1.jpg" class="grails-logo"/>
    </div>
</div>

    <div id="content" role="main">
        <section class="row colset-2-its">
          <h1>LeCoinCoin</h1>

            <p>
                Avec "lecoinoin", trouvez la bonne affaire sur le site référent de petites annonces de particulier à particulier et de professionnels. Avec des millions de petites annonces, trouvez la bonne occasion dans nos catégories voiture, immobilier, emploi, vacances, mode, maison, jeux vidéo, etc... Déposez une annonce gratuite en toute simplicité pour vendre, rechercher, donner vos biens de seconde main ou promouvoir vos services. Achetez en toute sécurité avec notre système de paiement en ligne et de livraison pour les annonces éligibles.
            </p>
<br>

        </section>
    </div>

</body>
</html>
