package tp1

class SaleAd {

    String title
    String description
    String longDescription

    Date dateCreated
    Date lastUpdated

    Float price

    List illustrations

    static hasMany = [illustrations: Illustration]

    static belongsTo = [author:User]


    static constraints = {
        title blank: false, nullable: false
        description blank: false, nullable:false
        longDescription blank: false, nullable: false
        illustrations nullable:true
        price min: 0F
    }
    static mapping = {
        longDescription type: 'text'
    }
    @Override
    String toString(){
        return title
    }
}