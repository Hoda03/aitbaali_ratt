package tp1
class Illustration {
    String filename
    static belongsTo = [saleAd: SaleAd]
    static constraints = {
        filename bank: false, nullable: false
    }
}
