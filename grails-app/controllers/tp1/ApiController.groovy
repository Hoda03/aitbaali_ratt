package tp1

import grails.converters.JSON
import grails.converters.XML


class ApiController {
    SaleAdService saleAdService

    def index() {
        render "OK"
    }
    def users() {
        switch (request.getMethod()) {
            case "GET":
                withFormat {
                    json { render User.list() as JSON }
                    xml { render User.list() as XML }
                }
                break
        }
    }
    def user() {
        switch (request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = 400
                def user = User.get(params.id)
                if (!user)
                    return response.status = 404
                response.withFormat {
                    json { render user as JSON }
                    xml { render user as XML }

                }
                break
            case "POST":
                if (!request.JSON.username || !request.JSON.password)
                    return response.status = 401
                def newUser = new User(username: request.JSON.username, password: request.JSON.password)
                newUser.save(flush:true)
                def role = Role.get(request.JSON.role)
                UserRole.create(newUser,role,true)
                return response.status = 201
                break

            case "PUT":
                if (!params.id)
                    return response.status = 400
                def user = User.get(params.id)
                if (request.JSON.username && request.JSON.password && user)
                    user.username = request.JSON.username
                user.password = request.JSON.password
                user.save(flush: true)
                return response.status = 201

                break

            case "PATCH":
                if (!params.id)
                    return response.status = 400
                def user = User.get(params.id)
                if (user)

                    user.properties = request.JSON
                user.save(flush: true)
                return response.status = 201

                break

            case "DELETE":
                if (!params.id)
                    return response.status = 400
                def user = User.get(params.id)
                if (!user)
                    return response.status = 404
                def listRole = UserRole.findAllByUser(user)
                UserRole.deleteAll(listRole)
                user.delete(flush: true)
                return response.status = 201
                break
            default:
                return response.status = 405
                break
        }
    }

    def SaleAd() {
        switch (request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = 400
                def SaleAd = SaleAd.get(params.id)
                if (!SaleAd)
                    return response.status = 404
                response.withFormat {
                    json { render SaleAd as JSON }
                    xml { render SaleAd as XML }
                }
                break
            case "PUT":
                if (!params.id)
                    return response.status = 400
                def saleAd = SaleAd.get(params.id)
                println saleAd
                if (!saleAd)
                    return response.status = 404
                println request.JSON
                saleAd.description = request.JSON.description
                saleAd.longDescription = request.JSON.longDescription
                saleAd.title = request.JSON.title
                saleAd.price = request.JSON.price
                saleAdService.save(saleAd)
                return response.status = 200
                break
            case "PATCH":
                if (!params.id)
                    return response.status = 400
                def saleAd = SaleAd.get(params.id)
                println saleAd
                if (!saleAd)
                    return response.status = 404
                if (request.JSON.description)
                    saleAd.description = request.JSON.description
                if (request.JSON.longDescription)
                    saleAd.longDescription = request.JSON.longDescription
                if (request.JSON.title)
                    saleAd.title = request.JSON.title
                if (request.JSON.price)
                    saleAd.price = request.JSON.price
                saleAdService.save(saleAd)
                return response.status = 200
                break
            case "DELETE":
                if (!params.id)
                    return response.status = 400
                def saleAd = saleAd.get(params.id)
                if (!saleAd)
                    return response.status = 404
                saleAd.delete(flush: true)
                return response.status = 200
                break
            default:
                return response.status = 405
                break
        }
        return response.status = 406
    }

    def saleAds() {

        switch (request.getMethod()) {

            case 'POST':
                if (!request.JSON.title || !request.JSON.description || !request.JSON.longDiscription || !request.JSON.price) {
                    println(request.JSON.description)
                    println(request.JSON.longDiscription)
                    return response.status = 401
                }


                def newSaleAd = new SaleAd(descShort: request.JSON.discription, descLong: request.JSON.longDiscription, title: request.JSON.title, price: request.JSON.price)
                newSaleAd.save(flush: true)
                return response.status = 201
                break
            case "GET":
                withFormat {
                    json { render SaleAd.list() as JSON }
                    xml { render SaleAd.list() as XML }
                }


        }
    }
}



