

### What has been achieved:
- CRUD:
    - User
    - Advertisement
- Display restriction:
  - Display of users for administrators and moderators
  - Posting of announcements for everyone

- Restriction of modifications
  - User modification for administrators and moderators
  - Modification of announcements for administrators and moderators

- API
  - CRUD:
    - User
    - Advertisement


### Running APP
to run the application you need to install Docker first, cause we used an Apache Http server docker image to save illustrations added at run time.

run the following command to build the docker image and run it.

creating image
```bash
docker build . -t my-apache2
```
running the docker image
```bash
docker run -dit --name my-running-app -p 8080:80 -v "%cd%/apache2sharedfolder":/usr/local/apache2/htdocs my-apache2
```

you should also change the variable tp1->illustrations->path of in the file ```grails-app\conf\application.yml``` to
```absolute-path-to-the-root-folder-of-the-app\apache2sharedfolder\ ```

